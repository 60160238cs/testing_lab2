/*const isUserNameValid = function(username){
    return true;
};*/

module.exports = {
    isUserNameValid: function(username){
        if(username.length<3 || username.length>15){
            return false;
        }

        if(username.toLowerCase() !== username){
            return false;
        }
        return true;
    },

    isAgeValid: function(age){
        var a = parseInt(age);
        if(Number.isNaN(a)){
            return false;
        }
        if(typeof a!=='number'){
            return false;
        }
        if(a<18 || a>100 ){
            return false;
        }
        return true;
    },

    isPasswordValid: function(password){
        if(typeof password ==='number'){
            return false;
        }
        if(password.length<8){
            return false;
        }
        
        var countLetter = 0;
        var letter = '!@#$%^&*()_+|~-=\\`{}[]:";\'<>?,./';
        for (var i = 0; i < password.length; i++){
            for (var j = 0; j < letter.length; j++){
                if(password.charAt(i) == letter.charAt(j)){
                    countLetter += 1;
                }
            }
        }
        if(countLetter<1){
            return false;
        }

        var countBigCharecter = 0;
        var countNumber = 0;
        for (var i = 0; i < password.length; i++) {
            var a = parseInt(password.charAt(i));

            if(password.charAt(i) == password.charAt(i).toUpperCase()){
                countBigCharecter += 1;
            }
            if(!Number.isNaN(a)){
                countNumber += 1;
            }
            
        }
        if((countBigCharecter)<1){
            return false;
        }
        if(countNumber<3){
            return false;
        }
        return true;
    },
    isDateValid: function(day, month, year){
        var typeOfyear = 'normal';
        if(year%100==0 && year%400==0){
            typeOfyear = 'unNormal';
        }
        if(day<1 || day>31){
            return false;
        }
        if(month<1 || month>12){
            return false;
        }
        if(year<1970 || year>2020){
            return false;
        }
        
        if(typeOfyear == 'normal'){
            if(month==2 && day>28){
                return false;
            }
            else if(month==4 && day>30){
                return false;
            }
            else if(month==6 && day>30){
                return false;
            }
            else if(month==9 && day>30){
                return false;
            }
            else if(month==11 && day>30){
                return false;
            }
        }
        else if(typeOfyear == 'unNormal'){
            if(month==2 && day>29){
                return false;
            }
            else if(month==4 && day>30){
                return false;
            }
            else if(month==6 && day>30){
                return false;
            }
            else if(month==9 && day>30){
                return false;
            }
            else if(month==11 && day>30){
                return false;
            }
        }
        return true;
    }

}
